# PGS Catalog template validator

Standalone Python validator for the **PGS Catalog Curation Template** (Metadata)

For more information about the PGS Catalog submission and its template, go to this page: [https://www.pgscatalog.org/submit/](https://www.pgscatalog.org/submit/)

## Example of command
```
python pgs_metadata_validator.py -f <my_template_file>.xlsx
```
